@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Profile</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item">Profile</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">Hai, {{ Auth::user()->nama }}!</h2>
            <p class="section-lead">
                Ubah informasi data diri disini.
            </p>

            <div class="row mt-sm-4">
                <div class="col-12 col-md-12 col-lg-5">
                    <div class="card profile-widget">
                        <div class="profile-widget-header">
                            @if ($user->foto)
                                <img alt="image" src="{{ asset('img/fotousers/' . $user->foto) }}"
                                    class="rounded-circle profile-widget-picture" style="height:100px">
                            @else
                                <img alt="image" src="{{ asset('img/avatar/avatar-3.png') }}"
                                    class="rounded-circle profile-widget-picture">
                            @endif
                            <div class="profile-widget-items">
                                <div class="profile-widget-item">
                                    <div class="profile-widget-item-label">Email</div>
                                    <div class="profile-widget-item-value">{{ $user->email }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="profile-widget-description">
                            <div class="profile-widget-name">{{ $user->nama }} <div
                                    class="text-muted d-inline font-weight-normal">
                                    <div class="slash"></div> {{ $user->jabatan }}
                                </div>
                            </div>
                            <div class="profile-widget-items">
                                <div class="profile-widget-item d-flex justify-content-between">
                                    <div class="profile-widget-item-label">NIP</div>
                                    <div class="profile-widget-item-value">{{ $user->nip }}</div>
                                </div>
                            </div>
                            <div class="profile-widget-items">
                                <div class="profile-widget-item d-flex justify-content-between">
                                    <div class="profile-widget-item-label">No. Telp/HP</div>
                                    <div class="profile-widget-item-value">{{ $user->telp }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-7">
                    <div class="card">
                        <form method="post" action="{{ url('/profile/update/'. $user->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="card-header">
                                <h4>Edit Profile</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required">Nama</label>
                                        <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                            name="nama" value="{{ old('nama', $user->nama) }}" id="nama" required>
                                        @error('nama')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required">NIP</label>
                                        <input type="text" class="form-control @error('nip') is-invalid @enderror"
                                                name="nip" value="{{ old('nip', $user->nip) }}" id="nip" required>
                                            @error('nip')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required">Email</label>
                                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                                                name="email" value="{{ old('email', $user->email) }}" id="email"
                                                required>
                                            @error('email')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                    </div>
                                    <div class="form-group col-md-6 col-12">
                                        <label>Password</label>
                                        <div class="input-group">
                                            <input type="password" id="password"
                                                class="form-control @error('password') is-invalid @enderror" name="password"
                                                value="{{ old('password') }}">
                                            <div class="input-group-append">
                                                <span id="mybutton" onclick="showPassword()" class="input-group-text">
                                                    <i class="fas fa-eye"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <small id="passwordHelpBlock" class="form-text text-muted">
                                            Diisi hanya jika ingin mengubah password.</small>
                                            @error('password')
                                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                            @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required">Telepon</label>
                                        <input type="tel" class="form-control @error('telp') is-invalid @enderror"
                                                name="telp" value="{{ old('telp', $user->telp) }}" id="telp"
                                                required>
                                            @error('telp')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                    </div>
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required">Jabatan</label>
                                        <input type="text"
                                                class="form-control @error('jabatan') is-invalid @enderror" name="jabatan"
                                                value="{{ old('jabatan', $user->jabatan) }}" id="jabatan" required>
                                            @error('jabatan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-12">
                                        <label>Foto Profil</label>
                                        <input type="hidden" name="oldImage" value="{{ $user->foto }}">
                                        <div class="col-sm-12 col-md-9">
                                            <div id="image-preview" class="image-preview" style="background-image: url('{{ $user->foto ? asset('img/fotousers/'. $user->foto) : '' }}'); background-size: cover; background-position:center" >
                                                <label for="image-upload" id="image-label">Choose File</label>
                                                <input type="file" name="image" id="image-upload" />
                                            </div>
                                            @error('image')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="password_lama" value="{{ $user->password }}">
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/opd/detail/{{ $opd->id }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Edit User</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/opd">OPD</a></div>
                <div class="breadcrumb-item"><a href="/opd/detail/{{ $opd->id }}">Detail OPD</a></div>
                <div class="breadcrumb-item">Edit User</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Form Edit User</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ url('opd/user/update/' . $user->id) }}" method="post" class="row"
                                enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="col-md-6">
                                    <div class="form-group row mb-4">
                                        <label
                                            class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama</label>
                                        <div class="col-sm-12 col-md-9">
                                            <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                                name="nama" value="{{ old('nama', $user->nama) }}" id="nama"
                                                required>
                                            @error('nama')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label
                                            class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">NIP</label>
                                        <div class="col-sm-12 col-md-9">
                                            <input type="text" class="form-control @error('nip') is-invalid @enderror"
                                                name="nip" value="{{ old('nip', $user->nip) }}" id="nip" required>
                                            @error('nip')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label
                                            class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Email</label>
                                        <div class="col-sm-12 col-md-9">
                                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                                name="email" value="{{ old('email', $user->email) }}" id="email"
                                                required>
                                            @error('email')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label
                                            class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Password</label>
                                        <div class="col-sm-12 col-md-9">
                                            <input type="password"
                                                class="form-control @error('password') is-invalid @enderror" name="password"
                                                value="{{ old('password', $user->password) }}" id="password" required>
                                            @error('password')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label
                                            class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Telepon</label>
                                        <div class="col-sm-12 col-md-9">
                                            <input type="tel" class="form-control @error('telp') is-invalid @enderror"
                                                name="telp" value="{{ old('telp', $user->telp) }}" id="telp"
                                                required>
                                            @error('telp')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label
                                            class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Jabatan</label>
                                        <div class="col-sm-12 col-md-9">
                                            <input type="text"
                                                class="form-control @error('jabatan') is-invalid @enderror" name="jabatan"
                                                value="{{ old('jabatan', $user->jabatan) }}" id="jabatan" required>
                                            @error('jabatan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Foto
                                            Profil</label>
                                        <input type="hidden" name="oldImage" value="{{ $user->foto }}">
                                        <div class="col-sm-12 col-md-9">
                                            <div id="image-preview" class="image-preview" style="background-image: url('{{ $user->foto ? asset('img/fotousers/'. $user->foto) : '' }}'); background-size: cover; background-position:center" >
                                                <label for="image-upload" id="image-label">Choose File</label>
                                                <input type="file" name="image" id="image-upload" />
                                            </div>
                                            @error('image')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    @if ($opd->id == 1)
                                        <div class="form-group row mb-4">
                                            <label
                                                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Role</label>
                                            <div class="col-sm-12 col-md-7">
                                                <select class="form-control selectric" name="role_id">
                                                    @foreach ($roles as $role)
                                                        <option value="{{ $role->id }}" {{ $user->role_id == $role->id ? 'selected' : '' }}>{{ $role->nama_role }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @else
                                        <input type="hidden" name="role_id" value="2">
                                    @endif
                                    <input type="hidden" name="opd_kode" value="{{ $opd->id }}">
                                    <input type="hidden" name="password_lama" value="{{ $user->password }}">
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button type="submit" class="btn btn-primary"
                                                id="btn-simpan">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
@endsection
