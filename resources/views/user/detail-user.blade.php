@foreach ($listuser as $user)
    <div class="modal fade" tabindex="-1" role="dialog" id="detailUser{{ $user->id }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detail User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mt-sm-4">
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card profile-widget">
                                <div class="profile-widget-header">
                                    @if ($user->foto)
                                        <img alt="image" src="{{ asset('img/fotousers/' . $user->foto) }}"
                                            class="rounded-circle profile-widget-picture" style="height:100px">
                                    @else
                                        <img alt="image" src="{{ asset('img/avatar/avatar-3.png') }}"
                                            class="rounded-circle profile-widget-picture">
                                    @endif
                                    <div class="profile-widget-items">
                                        <div class="profile-widget-item">
                                            <div class="profile-widget-item-label">Email</div>
                                            <div class="profile-widget-item-value">{{ $user->email }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile-widget-description">
                                    <div class="profile-widget-name">{{ $user->nama }} <div
                                            class="text-muted d-inline font-weight-normal">
                                            <div class="slash"></div> {{ $user->jabatan }}
                                        </div>
                                    </div>
                                    <div class="profile-widget-items">
                                        <div class="profile-widget-item d-flex justify-content-between">
                                            <div class="profile-widget-item-label">NIP</div>
                                            <div class="profile-widget-item-value">{{ $user->nip }}</div>
                                        </div>
                                    </div>
                                    <div class="profile-widget-items">
                                        <div class="profile-widget-item d-flex justify-content-between">
                                            <div class="profile-widget-item-label">No. Telp/HP</div>
                                            <div class="profile-widget-item-value">{{ $user->telp }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
