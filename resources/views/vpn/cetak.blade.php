<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VPN</title>
</head>

<body>
    <div>
        <table>
            <tbody>
                <tr>
                    <th align="right">Sukoharjo, {{ date('d F Y', strtotime($vpn->created_at)) }}</th>
                </tr>
                <tr>
                    <th width="10%">Nomor</th>
                    <td width="2%">:</td>
                    <td>{{ $vpn->no_vpn }}</td>
                </tr>
                <tr>
                    <th>Lampiran</th>
                    <td>:</td>
                    <td></td>
                </tr>
                <tr>
                    <th>Hal</th>
                    <td>:</td>
                    <td>Permohonan {{ $vpn->jenis_permohonan }} VPN</td>
                </tr>
            </tbody>
        </table>
    </div>

    <p align="justify" style="text-indent: 20px">
        Untuk meningkatkan pelayanan Pemerintah kepada masyarakat dalam rangka meningkatkan e-government yang baik dan
        terarah melalui jaringan internet dan aplikasi berbasis teknologi digital, kami bermaksud mengajukan Permohonan
        {{ $vpn->jenis_permohonan }} VPN {{ $vpn->user->opd->nama }} sebagai media komunikasi dan informasi dengan
        masyarakat luas.
    </p>
    <p align="justify" style="text-indent: 20px;">
        Sehubungan dengan hal tersebut di atas, berikut kami sertakan data informasi sebagai berikut:
    </p>
    <div>
        <table border="1" cellpadding="4" width="100%">
            <tbody>
                <tr>
                    <th width="20%" style="background-color: rgb(177, 177, 177);">Nama Instansi</th>
                    <td width="80%">{{ $vpn->user->opd->nama }}</td>
                </tr>
                <tr>
                    <th width="20%" style="background-color: rgb(177, 177, 177);">Keperluan</th>
                    <td width="80%">{{ $vpn->keperluan }}</td>
                </tr>
                <tr>
                    <th width="20%" style="background-color: rgb(177, 177, 177);">Rentang waktu</th>
                    <td width="80%">{{ date('d M Y', strtotime($vpn->tgl_awal)) }} - {{ date('d M Y', strtotime($vpn->tgl_berakhir)) }}</td>
                </tr>
                @if ($vpn->jenis_permohonan == 'Ganti Password')
                    <tr>
                        <th style="background-color: rgb(177, 177, 177);">Permasalahan</th>
                        <td>{{ $vpn->permasalahan }}</td>
                    </tr>
                    <tr>
                        <th style="background-color: rgb(177, 177, 177);">IP VPN</th>
                        <td>{{ $vpn->ip_vpn }}</td>
                    </tr>
                @endif
                <tr>
                    <th style="background-color: rgb(177, 177, 177);">Password</th>
                    <td>{{ $vpn->password }}</td>
                </tr>
            </tbody>
        </table>
        <p align="justify" style="text-indent: 20px;">
            Demikian Permohonan ini disampaikan, atas perhatian dan kerjasamanya diucapkan terimakasih.
        </p>

        <table width="100%">
            <tbody>
                <tr>
                    <th width="60%"></th>
                    <th align="center" width="40%">Kepala {{ $vpn->user->opd->nama }} <br> Kabupaten Sukoharjo</th>
                </tr><br><br><br><br><br>
                <tr>
                    <th width="60%"></th>
                    <th align="center">(______________________________)</th>
                </tr>
                <tr>
                    <th width="60%"></th>
                    <th align="left">NIP. <span style="display: none">(___________________________)</span></th>
                </tr>

            </tbody>
        </table>
    </div>
</body>

</html>
