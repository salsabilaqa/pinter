@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Manajemen Hak Akses</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item">Manajemen Hak Akses</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Tujuan/Jenis Akses</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Tujuan/Jenis Akses</h4>
                            <div class="card-header-action">
                                <a href="" data-toggle="modal" data-target="#addTujuan"
                                    class="btn btn-primary btn-icon icon-right"><i class="fas fa-plus"></i>
                                    Tambah</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="myTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Jenis Akses</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($jenisakses as $jen)
                                            <tr>
                                                <td class="text-center">
                                                    {{ $loop->iteration }}
                                                </td>
                                                <td>{{ $jen->nama }}</td>
                                                <td>
                                                    <div class="float-center dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="" data-toggle="modal"
                                                                data-target="#editTujuan{{ $jen->id }}"
                                                                class="dropdown-item has-icon"><i
                                                                    class="fas fa-pencil-alt"></i>
                                                                Edit</a>
                                                            <div class="dropdown-divider"></div>
                                                            <form class="btn-delete"
                                                                action="/manajemen-akses/delete/{{ $jen->id }}"
                                                                method="post">
                                                                @method('delete')
                                                                @csrf
                                                                <a type="submit"
                                                                    class="dropdown-item has-icon text-danger">
                                                                    <i class="fas fa-trash-alt"></i> Delete
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal Add Tujuan/Jenis Akses -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addTujuan">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Tujuan/Jenis Akses</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Tulis tujuan atau jenis akses yang ingin ditambahkan.</p>
                    <form action="{{ url('/manajemen-akses/create') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <input type="text" class="form-control @error('nama') is-invalid @enderror"
                            name="nama" value="{{ old('nama') }}" id="nama" required>
                        @error('nama')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="submit" class="btn btn-primary btn-shadow" id="">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Edit Tujuan/Jenis Akses -->
    @foreach ($jenisakses as $jen)
    <div class="modal fade" tabindex="-1" role="dialog" id="editTujuan{{ $jen->id }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Tujuan/Jenis Akses</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Tulis tujuan atau jenis akses yang ingin diubah.</p>
                    <form action="{{ url('/manajemen-akses/update/'. $jen->id) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <input type="text" class="form-control @error('nama') is-invalid @enderror"
                            name="nama" value="{{ old('nama', $jen->nama) }}" id="nama" required>
                        @error('nama')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="submit" class="btn btn-primary btn-shadow" id="">Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach
@endsection
