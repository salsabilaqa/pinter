@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Organisasi Perangkat Daerah</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item">OPD</div>
            </div>
        </div>

        <div class="section-body">
            {{-- <h2 class="section-title">DataTables</h2>
      <p class="section-lead">
        We use 'DataTables' made by @SpryMedia. You can check the full documentation <a href="https://datatables.net/">here</a>.
      </p> --}}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>OPD</h4>
                            <div class="card-header-action">
                                <a href="/opd/add" class="btn btn-primary btn-icon icon-right"><i class="fas fa-plus"></i>
                                    Tambah</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="table-1">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Nama OPD</th>
                                            <th>Email</th>
                                            <th>Telepon</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($opd as $op)
                                            <tr>
                                                <td class="text-center">
                                                    {{ $loop->iteration }}
                                                </td>
                                                <td>{{ $op->nama }}</td>
                                                <td>{{ $op->email }}</td>
                                                <td>{{ $op->telp }}</td>
                                                <td>
                                                    <div class="float-center dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="/opd/detail/{{ $op->id }}"
                                                                class="dropdown-item has-icon"><i class="fas fa-eye"></i>
                                                                Detail</a>
                                                            <a href="/opd/edit/{{ $op->id }}"
                                                                class="dropdown-item has-icon"><i class="fas fa-pencil-alt"></i>
                                                                Edit</a>
                                                            <div class="dropdown-divider"></div>
                                                            <form class="btn-delete"
                                                                action="/opd/delete/{{ $op->id }}" method="post">
                                                                @method('delete')
                                                                @csrf
                                                                <a type="submit"
                                                                    class="dropdown-item has-icon text-danger">
                                                                    <i class="fas fa-trash-alt"></i> Delete
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection