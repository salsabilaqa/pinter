<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hosting</title>
</head>

<body>
    <div>
        <table border="0.5" cellpadding="4" width="100%">
            <tbody>
                <tr>
                    <th align="center" width="20%" rowspan="2"><img src="{{ public_path('img/kabsukoharjo.jpeg') }}" height="100px" alt=""></th>
                    <th align="center" width="40%" rowspan="2"><div style="vertical-align: middle"><h3>FORMULIR PERMOHONAN WEB HOSTING</h3></div></th>
                    <th align="left" width="40%" height="50px">Nomor : <div style="text-align: center">{{ $h->no_hosting }}</div></th>
                </tr>
                <tr>
                    <td>Tanggal : <div style="text-align: center">{{ date('d F Y', strtotime($h->created_at)) }}</div></td>
                </tr>
                <tr style="background-color: rgb(177, 177, 177);">
                    <th align="center" colspan="3"><b>PILIHAN PERMOHONAN SUB DOMAIN</b></th>
                </tr>
                <tr>
                    <th align="center" width="33%">{{ $h->jenis_permohonan == 'Baru' ? '[X]' : '' }} Baru
                    </th>
                    <th align="center" width="33%">
                        {{ $h->jenis_permohonan == 'Perubahan' ? '[X]' : '' }} Perubahan Subdomain</th>
                    <th align="center" width="34%">
                        {{ $h->jenis_permohonan == 'Penambahan' ? '[X]' : '' }} Penambahan Spesifikasi</th>
                </tr>
                <tr>
                    <th align="center" colspan="1" width="25%">Nama Instansi</th>

                    <td align="left" width="75%">{{ $h->user->opd->nama }}</td>

                </tr>
                <tr>
                    <th align="center" colspan="3" width="100%" style="background-color: rgb(177, 177, 177);">
                        <b>KEPALA ORGANISASI PERANGKAT DAERAH</b>
                    </th>
                </tr>
                <tr>
                    <th align="center" width="25%">Nama</th>

                    <td align="left" width="75%">{{ $h->nama_kepala }}</td>

                </tr>
                <tr>
                    <th align="center" width="25%">NIP</th>
                    <td align="left" width="75%">{{ $h->nip_kepala }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">Jabatan</th>
                    <td align="left" width="75%">Kepala Dinas</td>
                </tr>

                <tr>
                    <th align="center" colspan="3" width="100%" style="background-color: rgb(177, 177, 177);"><b>ADMIN ORGANISASI PERANGKAT DAERAH</b></th>
                </tr>
                <tr>
                    <th align="center" width="25%">Nama</th>
                    <td align="left" width="75%">{{ $h->developer->nama ?? '' }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">NIP</th>
                    <td align="left" width="75%">{{ $h->developer->nip }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">Jabatan</th>
                    <td align="left" width="75%">{{ $h->developer->jabatan }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">Email</th>
                    <td align="left" width="75%">{{ $h->developer->email }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">Phone</th>
                    <td align="left" width="75%">{{ $h->developer->phone }}</td>
                </tr>
                <tr>
                    <th align="center" colspan="3" width="100%" style="background-color: rgb(177, 177, 177);"><b>DESKRIPSI WEBSITE</b></th>
                </tr>
                <tr>
                    <td colspan="3">{{ $h->deskripsi_web }}</td>
                </tr>
                <tr>
                    <th align="center" colspan="3" style="background-color: rgb(177, 177, 177);"><b>NAMA SUB DOMAIN YANG DIMINTA</b></th>
                </tr>
                <tr>
                    <th align="center" width="25%">Sub Domain</th>
                    <td align="left" width="75%">{{ $h->subdomain_baru }}</td>
                </tr>
                @if ($h->subdomain_lama)
                <tr>
                    <th align="center" width="25%">Sub Domain Baru</th>
                    <td align="left" width="75%">{{ $h->subdomain_lama }}</td>
                </tr>
                @endif
                <tr>
                    <th align="center" width="25%">Jenis Hosting</th>
                    <td align="left" width="75%">{{ $h->jenis_hosting }}</td>
                </tr>
                @if ($h->jenis_hosting == 'VPS')
                <tr>
                    <th align="center" width="25%">Operating System</th>
                    <td align="left" width="75%">{{ $h->os }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">Processor</th>
                    <td align="left" width="75%">{{ $h->processor }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">RAM</th>
                    <td align="left" width="75%">{{ $h->ram }}</td>
                </tr>
                @endif
                <tr>
                    <th align="center" width="25%">Storage</th>
                    <td align="left" width="75%">{{ $h->storage }}</td>
                </tr>

                <tr>
                    <th align="center" colspan="3" width="100%" style="background-color: rgb(177, 177, 177);"><b>PERSETUJUAN</b></th>
                </tr>
                <tr>
                    <th colspan="3">Dengan ini saya menyatakan bahwa data diatas adalah benar. Saya bertindak atas
                        nama organisasi yang saya wakili dan saya setuju untuk mematuhi semua aturan yang ditentukan dan
                        berlaku bagi seluruh pengguna fasilitas layanan Hosting Dinas Kominfo Kabupaten Sukoharjo</th>
                </tr>
                <tr>
                    <th align="right" colspan="3" ><b>KEPALA INSTANSI <br><br><br><br><br>

                            {{ $h->nama_kepala }} <br> {{ $h->nip_kepala }}
                        </b></th>
                </tr>
            </tbody>
        </table>
    </div>
    {{-- <div>
        <table border="1" cellpadding="3" width="100%">
            <tbody>
                <tr style="background-color: rgb(177, 177, 177);">
                    <th align="center" colspan="3"><b>PILIHAN PERMOHONAN SUB DOMAIN</b></th>
                </tr>
                <tr>
                    <th align="center">{{ $h->jenis_permohonan == 'Baru' ? '[X]' : '' }} Baru
                    </th>
                    <th align="center">
                        {{ $h->jenis_permohonan == 'Perubahan' ? '[X]' : '' }} Perubahan Subdomain</th>
                    <th align="center">
                        {{ $h->jenis_permohonan == 'Penambahan' ? '[X]' : '' }} Penambahan Spesifikasi</th>
                </tr>
                <tr>
                    <th align="center" colspan="1" width="25%">Nama Instansi</th>

                    <td align="left" width="75%">{{ $h->user->opd->nama }}</td>

                </tr>
                <tr>
                    <th align="center" colspan="3" width="100%" style="background-color: rgb(177, 177, 177);">
                        <b>KEPALA ORGANISASI PERANGKAT DAERAH</b>
                    </th>
                </tr>
                <tr>
                    <th align="center" width="25%">Nama</th>

                    <td align="left" width="75%">{{ $h->nama_kepala }}</td>

                </tr>
                <tr>
                    <th align="center" width="25%">NIP</th>
                    <td align="left" width="75%">{{ $h->nip_kepala }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">Jabatan</th>
                    <td align="left" width="75%">Kepala Dinas</td>
                </tr>

                <tr>
                    <th align="center" colspan="3" width="100%" style="background-color: rgb(177, 177, 177);"><b>ADMIN ORGANISASI PERANGKAT DAERAH</b></th>
                </tr>
                <tr>
                    <th align="center" width="25%">Nama</th>
                    <td align="left" width="75%">{{ $h->developer->nama ?? '' }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">NIP</th>
                    <td align="left" width="75%">{{ $h->developer->nip }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">Jabatan</th>
                    <td align="left" width="75%">{{ $h->developer->jabatan }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">Email</th>
                    <td align="left" width="75%">{{ $h->developer->email }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">Phone</th>
                    <td align="left" width="75%">{{ $h->developer->phone }}</td>
                </tr>
                <tr>
                    <th align="center" colspan="3" width="100%" style="background-color: rgb(177, 177, 177);"><b>DESKRIPSI WEBSITE</b></th>
                </tr>
                <tr>
                    <td colspan="3">{{ $h->deskripsi_web }}</td>
                </tr>
                <tr>
                    <th align="center" colspan="3" style="background-color: rgb(177, 177, 177);"><b>NAMA SUB DOMAIN YANG DIMINTA</b></th>
                </tr>
                <tr>
                    <th align="center" width="25%">Sub Domain</th>
                    <td align="left" width="75%">{{ $h->subdomain_baru }}</td>
                </tr>
                @if ($h->subdomain_lama)
                <tr>
                    <th align="center" width="25%">Sub Domain Baru</th>
                    <td align="left" width="75%">{{ $h->subdomain_lama }}</td>
                </tr>
                @endif
                <tr>
                    <th align="center" width="25%">Jenis Hosting</th>
                    <td align="left" width="75%">{{ $h->jenis_hosting }}</td>
                </tr>
                @if ($h->jenis_hosting == 'VPS')
                <tr>
                    <th align="center" width="25%">Operating System</th>
                    <td align="left" width="75%">{{ $h->os }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">Processor</th>
                    <td align="left" width="75%">{{ $h->processor }}</td>
                </tr>
                <tr>
                    <th align="center" width="25%">RAM</th>
                    <td align="left" width="75%">{{ $h->ram }}</td>
                </tr>
                @endif
                <tr>
                    <th align="center" width="25%">Storage</th>
                    <td align="left" width="75%">{{ $h->storage }}</td>
                </tr>

                <tr>
                    <th align="center" colspan="3" width="100%" style="background-color: rgb(177, 177, 177);"><b>PERSETUJUAN</b></th>
                </tr>
                <tr>
                    <th colspan="3">Dengan ini saya menyatakan bahwa data diatas adalah benar. Saya bertindak atas
                        nama organisasi yang saya wakili dan saya setuju untuk mematuhi semua aturan yang ditentukan dan
                        berlaku bagi seluruh pengguna fasilitas layanan Hosting Dinas Kominfo Kabupaten Sukoharjo</th>
                </tr>
                <tr>
                    <th align="right" colspan="3" ><b>KEPALA INSTANSI <br><br><br><br><br>

                            {{ $h->nama_kepala }} <br> {{ $h->nip_kepala }}
                        </b></th>
                </tr>
            </tbody>
        </table>
    </div> --}}
</body>

</html>
