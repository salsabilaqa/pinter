@extends('layouts.home')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h6 class="font-weight-bold">Jenis Server</h6>
                            </div>
                            <!-- Button trigger modal -->
                            <div class="col-6 text-end">
                                <button type="button" class="btn btn-sm bg-gradient-dark mb-0" data-bs-toggle="modal"
                                    data-bs-target="#addJenisServer"><i class="fas fa-plus"></i>
                                    Tambah
                                </button>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>
                    <div class="card-body px-0 pt-3 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead class="text-left">
                                    <tr>
                                        <th
                                            class="text-uppercase text-center text-dark text-xs font-weight-bolder opacity-7">
                                            No</th>
                                        <th class="text-uppercase text-dark text-xs font-weight-bolder opacity-7">Nama
                                            Domain</th>
                                        <th class="text-uppercase text-dark text-xs font-weight-bolder opacity-7">IP Address
                                        </th>
                                        <th
                                            class="text-uppercase text-dark text-xs text-center font-weight-bolder opacity-7">
                                            Location</th>
                                        <th
                                            class="text-center text-uppercase text-dark text-xs font-weight-bolder opacity-7">
                                            Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="d-flex justify-content-center px-4 py-1">
                                                <h6 class="mb-0 text-sm">1</h6>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex px-3 py-1">
                                                <h6 class="mb-0 text-sm">siberia</h6>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex px-3 py-1">
                                                <h6 class="mb-0 text-sm">192.206.198.203</h6>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <h6 class="mb-1 px-3 text-sm">Cpanel</h6>
                                        </td>
                                        <td class="d-flex justify-content-center">
                                            <a class="btn btn-light text-primary btn-sm btn-icon-only mx-1 my-2"
                                                data-placement="top" title="Detail" data-original-title="Detail">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            <a class="btn btn-light text-info btn-sm btn-icon-only mx-1 my-2"
                                                data-toggle="modal" data-placement="top" title="Edit"
                                                data-original-title="Edit" data-target="#editModalTP11">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <form class="btn-delete"
                                                action="http://tlhp.inspektorat.sukoharjokab.go.id/Delete-tp1/1"
                                                method="POST">
                                                <input type="hidden" name="_method" value="delete"> <input type="hidden"
                                                    name="_token" value="Sc0eIHVaNvRqJdpFtCBkfKehx5DuWwFft040pc88"> <button
                                                    type="submit"
                                                    class="btn btn-light text-danger btn-sm btn-icon-only mx-1 my-2"
                                                    data-placement="top" title="Delete" data-original-title="Delete"><i
                                                        class="fas fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addjenisserver" tabindex="-1" role="dialog" aria-labelledby="Modaljenisserver"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="font-weight-bold modal-title" id="modaljenisserver" style="padding-left: 35%">
                        Jenis Server</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="name" class="col-form-label text-sm">Nama Domain</label>
                            <input type="text" class="form-control" value="" id="name">
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-form-label text-sm">IP Address</label>
                            <input type="text" class="form-control" value="" id="name">
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-form-label text-sm">Location</label>
                            <input type="text" class="form-control" value="" id="name">
                        </div>

                    </form>
                </div>
                <div class="modal-footer" align="center">
                    <button type="button" class="btn bg-gradient-danger" data-bs-dismiss="modal">Tutup</button>
                    <button type="button" class="btn bg-gradient-info">Kirim</button>
                </div>
            </div>
        </div>
    </div>
@endsection
