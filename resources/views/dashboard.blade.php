@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats">
                        <div class="card-stats-title">Hosting {{ $tahun }}
                            <div class="dropdown d-inline">
                            </div>
                        </div>
                        <div class="card-stats-items">
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $hosting }}</div>
                                <div class="card-stats-item-label">Pending</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $h_proses }}</div>
                                <div class="card-stats-item-label">Proses</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $h_selesai }}</div>
                                <div class="card-stats-item-label">Selesai</div>
                            </div>
                        </div>
                    </div>
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-archive"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Hosting</h4>
                        </div>
                        <div class="card-body">
                            {{ $h_total }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats">
                        <div class="card-stats-title">Email {{ $tahun }}
                        </div>
                        <div class="card-stats-items">
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $email }}</div>
                                <div class="card-stats-item-label">Pending</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $e_proses }}</div>
                                <div class="card-stats-item-label">Proses</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $e_selesai }}</div>
                                <div class="card-stats-item-label">Selesai</div>
                            </div>
                        </div>
                    </div>
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-archive"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Email</h4>
                        </div>
                        <div class="card-body">
                            {{ $e_total }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats">
                        <div class="card-stats-title">VPN {{ $tahun }}
                        </div>
                        <div class="card-stats-items">
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $vpn }}</div>
                                <div class="card-stats-item-label">Pending</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $v_proses }}</div>
                                <div class="card-stats-item-label">Proses</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $v_selesai }}</div>
                                <div class="card-stats-item-label">Selesai</div>
                            </div>
                        </div>
                    </div>
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-archive"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total VPN</h4>
                        </div>
                        <div class="card-body">
                            {{ $v_total }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h4>Grafik {{ $tahun }}</h4>
                    </div>
                    <div class="card-body">
                        <canvas id="chart-line" height="163"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card card-statistic-2">
                            <div class="card-stats">
                                <div class="card-stats-title">Hak Akses {{ $tahun }}
                                    <div class="dropdown d-inline">
                                    </div>
                                </div>
                                <div class="card-stats-items">
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $hakakses }}</div>
                                        <div class="card-stats-item-label">Pending</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $k_proses }}</div>
                                        <div class="card-stats-item-label">Proses</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $k_selesai }}</div>
                                        <div class="card-stats-item-label">Selesai</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-icon shadow-primary bg-primary">
                                <i class="fas fa-archive"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>Total Hak Akses</h4>
                                </div>
                                <div class="card-body">
                                    {{ $k_total }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card card-statistic-2">
                            <div class="card-stats">
                                <div class="card-stats-title">Ruang Server {{ $tahun }}
                                    <div class="dropdown d-inline">
                                    </div>
                                </div>
                                <div class="card-stats-items">
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $ruang }}</div>
                                        <div class="card-stats-item-label">Pending</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $r_proses }}</div>
                                        <div class="card-stats-item-label">Proses</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $r_selesai }}</div>
                                        <div class="card-stats-item-label">Selesai</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-icon shadow-primary bg-primary">
                                <i class="fas fa-archive"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>Total Ruang Server</h4>
                                </div>
                                <div class="card-body">
                                    {{ $r_total }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- begin::ChartJS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
    <script>
        var hostingData = [{!! json_encode($hosting) !!}, {!! json_encode($h_proses) !!}, {!! json_encode($h_selesai) !!}, {!! json_encode($h_ditolak) !!}];
        var emailData = [{!! json_encode($email) !!}, {!! json_encode($e_proses) !!}, {!! json_encode($e_selesai) !!}, {!! json_encode($e_ditolak) !!}];
        var vpnData = [{!! json_encode($vpn) !!}, {!! json_encode($v_proses) !!}, {!! json_encode($v_selesai) !!}, {!! json_encode($v_ditolak) !!}];
        var ctx = document.getElementById('chart-line');
        var chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: {!! json_encode($status->pluck('status')) !!},
                datasets: [{
                        label: 'Hosting',
                        data: hostingData,
                        backgroundColor: 'rgba(255, 248, 0, 1.0)',
                        borderColor: 'rgba(255, 248, 0, 1.0)',
                        borderWidth: 1
                    },
                    {
                        label: 'Email',
                        data: emailData,
                        backgroundColor: 'rgba(124, 207, 0, 1.0)',
                        borderColor: 'rgba(124, 207, 0, 1.0)',
                        borderWidth: 1
                    },
                    {
                        label: 'VPN',
                        data: vpnData,
                        backgroundColor: 'rgba(32, 11, 170, 1.0)',
                        borderColor: 'rgba(32, 11, 170, 1.0)',
                        borderWidth: 1
                    },
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection
