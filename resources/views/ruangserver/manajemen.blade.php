@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Manajemen Ruang Server</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item">Manajemen Ruang Server</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Kartu Identitas</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Kartu Identitas</h4>
                            <div class="card-header-action">
                                <a href="" data-toggle="modal" data-target="#addKartuid"
                                    class="btn btn-primary btn-icon icon-right"><i class="fas fa-plus"></i>
                                    Tambah</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="myTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Jenis Kartu</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($kartus as $kartu)
                                            <tr>
                                                <td class="text-center">
                                                    {{ $loop->iteration }}
                                                </td>
                                                <td>{{ $kartu->jenis }}</td>
                                                <td>
                                                    <div class="float-center dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="" data-toggle="modal"
                                                                data-target="#editKartuid{{ $kartu->id }}"
                                                                class="dropdown-item has-icon"><i
                                                                    class="fas fa-pencil-alt"></i>
                                                                Edit</a>
                                                            <div class="dropdown-divider"></div>
                                                            <form class="btn-delete"
                                                                action="/manajemen-server/kartuid/delete/{{ $kartu->id }}"
                                                                method="post">
                                                                @method('delete')
                                                                @csrf
                                                                <a type="submit"
                                                                    class="dropdown-item has-icon text-danger">
                                                                    <i class="fas fa-trash-alt"></i> Delete
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">Aktivitas</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h4>Aktivitas</h4>
                            <div class="card-header-action">
                                <a href="" data-toggle="modal" data-target="#addAktivitas"
                                    class="btn btn-primary btn-icon icon-right"><i class="fas fa-plus"></i>
                                    Tambah</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="myTable2">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Aktivitas</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($aktivitases as $aktivitas)
                                            <tr>
                                                <td class="text-center">
                                                    {{ $loop->iteration }}
                                                </td>
                                                <td>{{ $aktivitas->nama }}</td>
                                                <td>
                                                    <div class="float-center dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="" data-toggle="modal"
                                                                data-target="#editAktivitas{{ $aktivitas->id }}"
                                                                class="dropdown-item has-icon"><i
                                                                    class="fas fa-pencil-alt"></i>
                                                                Edit</a>
                                                            <div class="dropdown-divider"></div>
                                                            <form class="btn-delete"
                                                                action="/manajemen-server/aktivitas/delete/{{ $aktivitas->id }}"
                                                                method="post">
                                                                @method('delete')
                                                                @csrf
                                                                <a type="submit"
                                                                    class="dropdown-item has-icon text-danger">
                                                                    <i class="fas fa-trash-alt"></i> Delete
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal Add Kartu Identitas -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addKartuid">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Kartu Identitas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Tulis nama atau jenis kartu identitas yang ingin ditambahkan.</p>
                    <form action="{{ url('/manajemen-server/kartuid/create') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <input type="text" class="form-control @error('jenis') is-invalid @enderror"
                            name="jenis" value="{{ old('jenis') }}" id="jenis" required>
                        @error('jenis')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="submit" class="btn btn-primary btn-shadow" id="">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Add Aktivitas -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addAktivitas">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Aktivitas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Tulis nama aktivitas yang ingin ditambahkan.</p>
                    <form action="{{ url('/manajemen-server/aktivitas/create') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <input type="text" class="form-control @error('nama') is-invalid @enderror"
                            name="nama" value="{{ old('nama') }}" id="nama" required>
                        @error('nama')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="submit" class="btn btn-primary btn-shadow" id="">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Edit Kartu Identitas -->
    @foreach ($kartus as $kartu)
    <div class="modal fade" tabindex="-1" role="dialog" id="editKartuid{{ $kartu->id }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Kartu Identitas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Tulis nama atau jenis kartu identitas yang ingin diubah.</p>
                    <form action="{{ url('/manajemen-server/kartuid/update/'. $kartu->id) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <input type="text" class="form-control @error('jenis') is-invalid @enderror"
                            name="jenis" value="{{ old('jenis', $kartu->jenis) }}" id="jenis" required>
                        @error('jenis')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="submit" class="btn btn-primary btn-shadow" id="">Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach

    <!-- Modal Edit Aktivitas -->
    @foreach ($aktivitases as $aktivitas)
    <div class="modal fade" tabindex="-1" role="dialog" id="editAktivitas{{ $aktivitas->id }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Aktivitas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Tulis nama aktivitas yang ingin diubah.</p>
                    <form action="{{ url('/manajemen-server/aktivitas/update/'. $aktivitas->id) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <input type="text" class="form-control @error('nama') is-invalid @enderror"
                            name="nama" value="{{ old('nama', $aktivitas->nama) }}" id="nama" required>
                        @error('nama')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="submit" class="btn btn-primary btn-shadow" id="">Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach
@endsection
