@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/ruangserver" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Permohonan Izin Akses Ruang Server</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/ruangserver">Ruang Server</a></div>
                <div class="breadcrumb-item">Permohonan Izin Akses Ruang Server</div>
            </div>
        </div>
        <h2 class="section-title">Form Permohonan Izin Akses Ruang Server</h2>
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <form action="{{ url('/ruangserver/proses/update/'. $rservers->id) }}" method="post">
                        @csrf
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Informasi Pemohon</h4>
                            </div>
                            <div class="card-body">
                                <input type="hidden" name="status" value="2">
                                {{-- <div class="form-group"> --}}
                                    {{-- <label for=""></label>
                                    <a href="javascript:" class="addpemohon btn btn-primary" style="float: right;">Tambah Pemohon</a>
                                </div> --}}
                                @foreach ($rservers->timpemohon as $pem)
                                <div class="mb-5 mt-5">
                                    <label class="font-weight-bold">Pemohon {{ $loop->iteration }}</label>
                                    <hr class="mt-0">
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text"
                                                class="form-control"
                                                name="nama[]" value="{{ old('nama', $pem->nama) }}" id="nama" readonly required>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Perusahaan</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text"
                                                class="form-control"
                                                name="perusahaan[]" value="{{ old('perusahaan', $pem->perusahaan) }}" id="perusahaan" readonly required>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jabatan</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text"
                                                class="form-control"
                                                name="jabatan[]" value="{{ old('jabatan', $pem->jabatan) }}" id="jabatan" readonly required>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group">
                                        <label for=""></label>
                                        <a href="javascript:" class="remove btn btn-danger" style="float: right;">Hapus</a>
                                    </div> --}}
                                </div>
                                @endforeach
                                <hr>
                                    <div class="pemohon"></div>
                                    <div class="form-group row mb-4">
                                        <label
                                            class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nomor KTP PIC</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control @error('no_ktp') is-invalid @enderror"
                                                name="no_ktp" value="{{ old('no_ktp', $rservers->no_ktp) }}" id="no_ktp" readonly required>
                                            @error('no_ktp')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="card card-danger">
                            <div class="card-header">
                                <h4>Rincian Permohonan</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tanggal</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="date" class="form-control @error('tanggal') is-invalid @enderror"
                                            name="tanggal" value="{{ old('tanggal', $rservers->tanggal) }}" id="tanggal" readonly required>
                                        @error('tanggal')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Waktu Datang</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="time" class="form-control @error('waktu_datang') is-invalid @enderror"
                                            name="waktu_datang" value="{{ old('waktu_datang', $rservers->waktu_datang) }}" id="waktu_datang" readonly required>
                                        @error('waktu_datang')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Waktu Meninggalkan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="time" class="form-control @error('waktu_meninggalkan') is-invalid @enderror"
                                            name="waktu_meninggalkan" value="{{ old('waktu_meninggalkan', $rservers->waktu_meninggalkan) }}" id="waktu_meninggalkan" readonly required>
                                        @error('waktu_meninggalkan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 d-block">Aktivitas</label>
                                    <div class="form-group">
                                        <div class="form-check">
                                      <input class="form-check-input @error('aktivitas') is-invalid @enderror" name="aktivitas[]" value="Pemasangan" {{ in_array('Pemasangan', explode(',', $rservers->aktivitas)) ? 'checked readonly disabled' : 'readonly disabled' }} type="checkbox" id="defaultCheck1">
                                      <label class="form-check-label" for="defaultCheck1">
                                        Pemasangan
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input @error('aktivitas') is-invalid @enderror" name="aktivitas[]" value="Perawatan" {{ in_array('Perawatan', explode(',', $rservers->aktivitas)) ? 'checked readonly disabled' : 'readonly disabled' }} type="checkbox" id="defaultCheck1">
                                      <label class="form-check-label" for="defaultCheck1">
                                        Perawatan
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input @error('aktivitas') is-invalid @enderror" name="aktivitas[]" value="Pembongkaran" {{ in_array('Pembongkaran', explode(',', $rservers->aktivitas)) ? 'checked readonly disabled' : 'readonly disabled' }} type="checkbox" id="defaultCheck1">
                                      <label class="form-check-label" for="defaultCheck1">
                                        Pembongkaran
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input @error('aktivitas') is-invalid @enderror" name="aktivitas[]" value="Perbaikan" {{ in_array('Perbaikan', explode(',', $rservers->aktivitas)) ? 'checked readonly disabled' : 'readonly disabled' }} type="checkbox" id="defaultCheck1">
                                      <label class="form-check-label" for="defaultCheck1">
                                        Perbaikan
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input @error('aktivitas') is-invalid @enderror" name="aktivitas[]" value="Setup" {{ in_array('Setup', explode(',', $rservers->aktivitas)) ? 'checked readonly disabled' : 'readonly disabled' }} type="checkbox" id="defaultCheck1">
                                      <label class="form-check-label" for="defaultCheck1">
                                        Setup
                                      </label>
                                    </div>
                                    @error('aktivitas')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 d-block">Izin Pengambilan Gambar</label>
                                    <div class="form-group">
                                    <div class="form-check">
                                      <input class="form-check-input @error('izin_foto') is-invalid @enderror" type="radio" name="izin_foto" value="Ya" {{ $rservers->izin_foto === 'Ya' ? 'checked readonly disabled' : 'readonly disabled' }} id="exampleRadios1">
                                      <label class="form-check-label" for="exampleRadios1">
                                        Ya
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input @error('izin_foto') is-invalid @enderror" type="radio" name="izin_foto" value="Tidak" {{ $rservers->izin_foto === 'Tidak' ? 'checked readonly disabled' : 'readonly disabled' }} id="exampleRadios2">
                                      <label class="form-check-label" for="exampleRadios2">
                                        Tidak
                                      </label>
                                    </div>
                                    @error('izin_foto')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Catatan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('catatan') is-invalid @enderror" style="height: auto" name="catatan" id="catatan" readonly>
                                            @if ($rservers->catatan)
                                                {{ old('catatan', $rservers->catatan) }}
                                            @else
                                                -
                                            @endif
                                        </textarea>
                                        @error('catatan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Diisi Oleh Petugas</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">
                                        Nomor Kartu Akses</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text"
                                            class="form-control @error('no_kartu_akses') is-invalid @enderror"
                                            name="no_kartu_akses" value="{{ old('no_kartu_akses', $rservers->no_kartu_akses) }}" id="no_kartu_akses" readonly required>
                                        @error('no_kartu_akses')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">
                                        Nomor Rak</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text"
                                            class="form-control @error('no_rak') is-invalid @enderror"
                                            name="no_rak" value="{{ old('no_rak', $rservers->no_rak) }}" id="no_rak" readonly required>
                                        @error('no_rak')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script>
        var button_add = document.getElementById('addpemohon');
        $('.addpemohon').on('click', function() {
            addpemohon();
        });

        function addpemohon() {
            var pemohon =
                '<div><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama</label><div class="col-sm-12 col-md-7"><input type="text" class="form-control" name="nama[]" value="{{ old('nama') }}" id="nama" required></div></div><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Perusahaan</label><div class="col-sm-12 col-md-7"><input type="text"class="form-control" name="perusahaan[]" value="{{ old('perusahaan') }}" id="perusahaan" required></div></div><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Jabatan</label><div class="col-sm-12 col-md-7"><input type="text" class="form-control" name="jabatan[]" value="{{ old('jabatan') }}" id="jabatan" required></div></div><label for=""></label><a href="javascript:" class="remove btn btn-danger" style="float: right;">Hapus</a></div></div>';
            $('.pemohon').append(pemohon);
        };
// document.get
        // if (pemohon => 4) {
        //     button_add.style.display = 'none';
        // }

        $('.remove').live('click', function() {
            $(this).parent().remove();
        })
        $('.hapus').live('click', function() {
            $(this).parent().parent().parent().remove();
        })

    </script>
@endsection
