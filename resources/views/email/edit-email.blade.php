@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/email" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Edit Permohonan Email</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/email">Email</a></div>
                <div class="breadcrumb-item">Edit Permohonan Email</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Form Edit Permohonan Email</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ url('email/update/'. $email->id) }}" method="post">
                                @method('put')
                                @csrf
                                @if ($email->jenis_permohonan == 'Perubahan')
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email Lama</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="input-group">
                                            <input type="text"
                                                class="form-control" id="email" name="email_lama" value="{{ $email->email_lama }}" readonly>
                                            <div class="input-group-append">
                                                <div class="input-group-text">@sukoharjokab.go.id</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama Email {{ $email->jenis_permohonan == 'Perubahan' ? 'Baru' : '' }}</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="input-group">
                                            <input type="text"
                                                class="form-control @error('nama_email') is-invalid @enderror"
                                                id="email" name="nama_email" value="{{ old('nama_email', $email->nama_email) }}" required {{ $email->jenis_permohonan == 'Ganti Password' ? 'readonly' : ''}}>
                                            <div class="input-group-append">
                                                <div class="input-group-text">@sukoharjokab.go.id</div>
                                            </div>
                                        </div>
                                        @error('nama_email')
                                            <div class="invalid-feedback d-block">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                @if ($email->jenis_permohonan == 'Baru')
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama
                                        Pengguna</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text"
                                            class="form-control @error('nama_pengguna') is-invalid @enderror"
                                            name="nama_pengguna" value="{{ old('nama_pengguna', $email->nama_pengguna) }}" id="nama_pengguna"
                                            required>
                                        @error('nama_pengguna')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                @endif
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Password</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="input-group">
                                            <input type="password" id="password"
                                                class="form-control @error('password') is-invalid @enderror" name="password"
                                                value="{{ old('password', $email->password) }}" required>
                                            <div class="input-group-append">
                                                <span id="mybutton" onclick="showPassword()" class="input-group-text">
                                                    <i class="fas fa-eye"></i>
                                                </span>
                                            </div>
                                        </div>
                                        @error('password')
                                            <div class="invalid-feedback d-block">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
