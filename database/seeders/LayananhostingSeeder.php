<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class LayananhostingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('layanan_hosting')->insert([[
            'user_id' => '2',
            'jenis_permohonan' => 'Baru',
            'nama_kepala' => 'bela',
            'nip_kepala' => '1234567890',
            'deskripsi_web' => 'lorem10 skdkdkdkdk akks',
            'jenis_hosting' => 'VPS',
            'os' => 'windows',
            'processor' => 'intel',
            'ram' => '2',
            'storage' => '500',
            'subdomain_lama' => '',
            'subdomain_baru' => 'pangan',
            'no_hosting' => 'HS-2023040500001',
            'is_changed' => '',
            'status' => '1',
            'keterangan' => '',
            'created_at' => '2023-04-1 12:13:00'
        ],
        [
            'user_id' => '2',
            'jenis_permohonan' => 'Baru',
            'nama_kepala' => 'bela',
            'nip_kepala' => '1234567890',
            'deskripsi_web' => 'lorem10 skdkdkdkdk akks',
            'jenis_hosting' => 'VPS',
            'os' => 'windows',
            'processor' => 'intel',
            'ram' => '2',
            'storage' => '500',
            'subdomain_lama' => '',
            'subdomain_baru' => 'pangan',
            'no_hosting' => 'HS-2023041000002',
            'is_changed' => '',
            'status' => '1',
            'keterangan' => '',
            'created_at' => '2023-04-1 12:13:00'
            ]]);
    }
}