<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PihakketigaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pihak_ketiga')->insert([[
            'nama_perusahaan' => 'Codeline Company',
            'nama' => 'Nishabel',
            'telp' => '081231885072',
            'jabatan' => 'Manager'
        ]]);
    }
}
