<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('email')->insert([[
            'user_id' => '2',
            'nama_email' => 'diskominfo',
            'password' => 'password123',
            'nama_pengguna' => 'Salsabila',
            'jenis_permohonan' => 'Baru',
            'no_email' => 'EM-2023040200001',
            'status' => '1',
            'is_changed' => '0',
            'created_at' => '2023-02-28 12:13:00'
        ], [
            'user_id' => '2',
            'nama_email' => 'coba2',
            'password' => 'passwordcoba543',
            'nama_pengguna' => 'Guide987',
            'jenis_permohonan' => 'Perubahan',
            'no_email' => 'EM-2023040500002',
            'status' => '1',
            'is_changed' => '0',
            'created_at' => '2023-02-28 12:13:00'
        ]]);
    }
}
