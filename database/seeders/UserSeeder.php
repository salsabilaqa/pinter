<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'nip' => '123456789',
            'nama' => 'User789',
            'email' => 'superadmin@gmail.com',
            // 'username' => 'User789',
            'password' => Hash::make('rahasia21'),
            'telp' => '081234567898',
            'jabatan' => 'Kepala Direktur',
            'foto' => 'avatar-1.png',
            'opd_kode' => '1',
            'role_id' => '1',
        ], [
            'nip' => '123456700',
            'nama' => 'User700',
            'email' => 'admin@gmail.com',
            // 'username' => 'User789',
            'password' => Hash::make('rahasia22'),
            'telp' => '081234567898',
            'jabatan' => 'Kepala Direktur',
            'foto' => 'avatar-5.png',
            'opd_kode' => '2',
            'role_id' => '2',
        ]]);
    }
}
