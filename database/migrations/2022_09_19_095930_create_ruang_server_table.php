<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ruang_server', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->cascadeOnUpdate()->nullOnDelete();
            $table->string('no_ktp');
            $table->date('tanggal');
            $table->time('waktu_datang')->nullable();
            $table->time('waktu_meninggalkan')->nullable();
            $table->string('no_rak')->nullable();
            $table->string('aktivitas');
            $table->string('no_kartu_akses')->nullable();
            $table->string('izin_foto');
            $table->foreignId('status')->nullable()->constrained('status')->cascadeOnUpdate()->nullOnDelete();
            $table->text('catatan')->nullable();
            $table->string('surat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ruang_server');
    }
};
