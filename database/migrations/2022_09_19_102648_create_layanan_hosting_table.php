
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanan_hosting', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->cascadeOnUpdate()->nullOnDelete();
            $table->string('jenis_permohonan')->nullable();
            $table->string('nama_kepala');
            $table->string('nip_kepala');
            $table->text('deskripsi_web')->nullable();
            $table->string('jenis_hosting');
            $table->string('os')->nullable();
            $table->string('processor')->nullable();
            $table->string('ram')->nullable();
            $table->string('storage')->nullable();
            $table->string('subdomain_lama')->nullable();
            $table->string('subdomain_baru');
            $table->string('no_hosting')->nullable();
            $table->string('surat')->nullable();
            $table->string('is_changed')->nullable();
            $table->string('keterangan')->nullable();
            $table->foreignId('status')->nullable()->constrained('status')->cascadeOnUpdate()->nullOnDelete();
            $table->integer('old_hosting_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layanan_hosting');
    }
};
