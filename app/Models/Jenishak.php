<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenishak extends Model
{
    use HasFactory;
    protected $table = 'jenis_hak_akses';
    protected $guarded = ['id'];

    public function hakakses()
    {
        return $this->hasMany(Hakakses::class);
    }
}
