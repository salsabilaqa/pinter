<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opd extends Model
{
    use HasFactory;
    protected $table = 'opd';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->hasMany(User::class, 'opd_kode');
    }

    public function hosting()
    {
        return $this->hasMany(Hosting::class);
    }

    public function ruangserver()
    {
        return $this->hasMany(Ruangserver::class);
    }
}
