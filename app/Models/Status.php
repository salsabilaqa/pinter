<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;
    protected $table = 'status';
    protected $guarded = ['id'];
    
    public function ruang(){
        return $this->hasMany(Ruangserver::class, 'status');
    }

    public function hosting()
    {
        return $this->hasMany(Hosting::class, 'status');
    }
}
