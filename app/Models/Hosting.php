<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Hosting extends Model
{
    use HasFactory;

    protected $table = 'layanan_hosting';
    protected $guarded = ['id'];

    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function statushosting(){
        return $this->belongsTo(Status::class, 'status');
    }

    public function developer()
    {
        return $this->hasOne(Devweb::class, 'hosting');
    }

    public static function generateNomor()
    {
        $tahun = date('Y');
        $bulan = date('m');
        $tanggal = date('d');
        $lastNoKar = self::where('no_hosting', 'like', '%'.$tahun.'%')->max('no_hosting');
        if ($lastNoKar) {
            $lastTahun = substr($lastNoKar, 3, 4);
            if ($lastTahun == $tahun) {
                $noUrut = intval(substr($lastNoKar, -5)) + 1;
            } else {
                $noUrut = 1;
            }
        } else {
            $noUrut = 1;
        }
        $noKar = 'HS-'. $tahun . $bulan . $tanggal . sprintf('%05d', $noUrut);
        return $noKar;
    }

}