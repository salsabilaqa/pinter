<?php

namespace App\Http\Controllers;

use App\Models\Opd;
use App\Models\Devweb;
use App\Models\Hosting;
use App\Models\Jenisserver;
use Illuminate\Http\Request;
use App\Models\Jenispermohonan;
use App\Models\Status;
use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HostingController extends Controller
{
    public function index(Request $request)
    {
        if ($request->tahun) {
            if (Auth::user()->role_id == 2) {
                $hosting = Hosting::with('statushosting', 'user', 'developer')->where('user_id', Auth::user()->id)->where(DB::raw('YEAR(created_at)'), $request->tahun)->latest()->get();
            } else {
                $hosting = Hosting::where(DB::raw('YEAR(created_at)'), $request->tahun)->latest()->get();
            }
        } else {
            if (Auth::user()->role_id == 2) {
                $hosting = Hosting::with('statushosting', 'user', 'developer')->where(DB::raw('YEAR(created_at)'), now())->latest()->get();
            } else {
                $hosting = Hosting::where(DB::raw('YEAR(created_at)'), now())->latest()->get();
            }
        }
        
        $tahun = DB::table('layanan_hosting')->select(DB::raw('YEAR(created_at) as tahun'))->orderBy(DB::raw('YEAR(created_at)'), 'desc')->groupBy(DB::raw('YEAR(created_at)'))->pluck('tahun');
       
        $data = [
            'listhosting' => $hosting,
            'status' => Status::all(),
            'tahun' => $tahun,
            'jenispermohon' => Hosting::select('jenis_permohonan')->groupBy('jenis_permohonan')->pluck('jenis_permohonan'),
        ]; 
        return view('Hosting.hosting', $data);
    }

    public function create()
    {
        $no_hosting = Hosting::generateNomor();
        return view('Hosting.create-hosting', [
            'no_hosting' => $no_hosting
        ]);
    }

    
    public function store(Request $request)
    {        
        $validator = Validator::make($request->all(), [
            'nama_kepala' => 'required',
            'nip_kepala' => 'required',
            'jenis_hosting' => 'required',
            'storage' => 'required',
            'nama' => 'required',
            'nip' => 'required',
            'jabatan' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'subdomain_baru' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        if ($request->jenis_hosting == 'VPS') {
            $hosting = Hosting::create([
                'nama_kepala' => $request->nama_kepala,
                'nip_kepala' => $request->nip_kepala,
                'jenis_permohonan' => $request->jenis_permohonan,
                'user_id' => $request->user,
                'deskripsi_web' => $request->deskripsi_web,
                'jenis_hosting' => $request->jenis_hosting,
                'subdomain_baru' => $request->subdomain_baru,
                'storage' => $request->storage,
                'status' => $request->status,
                'os' => $request->os,
                'processor' => $request->processor,
                'ram' => $request->ram,
                'no_hosting' => $request->no_hosting,
            ]);
        } elseif ($request->jenis_hosting == 'Cpanel') {
            $hosting = Hosting::create([
                'nama_kepala' => $request->nama_kepala,
                'nip_kepala' => $request->nip_kepala,
                'jenis_permohonan' => $request->jenis_permohonan,
                'user_id' => $request->user,
                'deskripsi_web' => $request->deskripsi_web,
                'jenis_hosting' => $request->jenis_hosting,
                'subdomain_baru' => $request->subdomain_baru,
                'storage' => $request->storage,
                'status' => $request->status,
                'os' => Null,
                'processor' => Null,
                'ram' => Null,
                'no_hosting' => $request->no_hosting,
            ]);
        }

        Devweb::create([
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jabatan' => $request->jabatan,
            'email' => $request->email,
            'phone' => $request->phone,
            'hosting' => $hosting->id,
        ]);
        
        return redirect('/hosting')->with('successPermohonan', 'Permohonan hosting baru berhasil dibuat!');
    }

    public function edit($id)
    {
        $data = [
            'hosting' => Hosting::with('statushosting', 'user', 'developer')->find($id),
        ];
        return view('Hosting.edit-hosting', $data);
    }

    public function detail($id)
    {
        $data = [
            'hosting' => Hosting::with('statushosting', 'user', 'developer')->find($id),
        ];
        return view('Hosting.detail-hosting', $data);
    }

    public function Update(Request $request, $id)
    {
        $hosting = Hosting::find($id);
        
        $rules = [
            'nama_kepala' => 'required',
            'nip_kepala' => 'required',
            'storage' => 'required',
            'nama' => 'required',
            'nip' => 'required',
            'jabatan' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'subdomain_baru' => 'required',
        ];
        if ($request->jenis_hosting) {
            $rules['jenis_hosting'] = 'required';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        if ($request->file('surat')) {
            $surat = date('Ymdhis') . '_' . $request->file('surat')->getClientOriginalName();
            $request->file('surat')->move('form_hosting/', $surat);
            if ($request->oldFile) {
                unlink('form_hosting/'. $request->oldFile);
            }
        } else {
            $surat = $request->oldFile;
        }

        if ($hosting->jenis_permohonan == 'Perubahan') {
            $hosting->update([
                'subdomain_baru' => $request->subdomain_baru,
                'surat' => $surat,
            ]);
        } else {
            if ($request->jenis_hosting == 'VPS') {
                $hosting->update([
                    'nama_kepala' => $request->nama_kepala,
                    'nip_kepala' => $request->nip_kepala,
                    'deskripsi_web' => $request->deskripsi_web,
                    'jenis_hosting' => $request->jenis_hosting,
                    'subdomain_baru' => $request->subdomain_baru,
                    'storage' => $request->storage,
                    'os' => $request->os,
                    'processor' => $request->processor,
                    'ram' => $request->ram,
                    'surat' => $surat,
                ]);
            } elseif ($request->jenis_hosting == 'Cpanel') {   
                $hosting->update([
                    'nama_kepala' => $request->nama_kepala,
                    'nip_kepala' => $request->nip_kepala,
                    'deskripsi_web' => $request->deskripsi_web,
                    'jenis_hosting' => $request->jenis_hosting,
                    'subdomain_baru' => $request->subdomain_baru,
                    'storage' => $request->storage,
                    'os' => Null,
                    'processor' => Null,
                    'ram' => Null,
                    'surat' => $surat
                ]);
            }
        }

        Devweb::where('hosting', $hosting->id)->update([
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jabatan' => $request->jabatan,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);

        return redirect('/hosting')->with('success', 'Permohonan hosting berhasil diubah!');
    }

    public function delete($id)
    {
        $hosting = Hosting::find($id);
        Devweb::where('hosting', $hosting->id)->delete();
        $hosting->delete();

        return redirect('/hosting')->with('success', 'Permohonan hosting berhasil dihapus!');
    }

    public function CreatePerubahan($id)
    {
        $no_hosting = Hosting::generateNomor();
        $data = [
            'hosting' => Hosting::with('statushosting', 'user', 'developer')->find($id),
            'no_hosting' => $no_hosting
        ];
        return view('Hosting.create-perubahan', $data);
    }

    public function perubahanUpdate(Request $request, $id)
    {
        $hosting = Hosting::find($id);
        
        $rules = [
            'subdomain_baru' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $hosting->update([
            'is_changed' => '1'
        ]);
        
        $hosting_baru = Hosting::create([
            'nama_kepala' => $request->nama_kepala,
            'nip_kepala' => $request->nip_kepala,
            'jenis_permohonan' => $request->jenis_permohonan,
            'user_id' => $request->user,
            'deskripsi_web' => $request->deskripsi_web,
            'jenis_hosting' => $request->jenis_hosting,
            'os' => $request->os,
            'processor' => $request->processor,
            'ram' => $request->ram,
            'subdomain_baru' => $request->subdomain_baru,
            'subdomain_lama' => $request->subdomain_lama,
            'storage' => $request->storage, 
            'status' => $request->status,
            'old_hosting_id' => $hosting->id,
            'no_hosting' => $request->no_hosting,
        ]);

        Devweb::create([
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jabatan' => $request->jabatan,
            'email' => $request->email,
            'phone' => $request->phone,
            'hosting' => $hosting_baru->id,
        ]);

        return redirect('/hosting')->with('successPermohonan', 'Permohonan perubahan subdomain berhasil dibuat!');
    }

    public function CreatePenambahan($id)
    {
        $no_hosting = Hosting::generateNomor();
        $data = [
            'hosting' => Hosting::with('statushosting', 'user', 'developer')->find($id),
            'no_hosting' => $no_hosting
        ];
        return view('Hosting.create-penambahan', $data);
    }

    public function penambahanUpdate(Request $request, $id)
    {
        $hosting = Hosting::find($id);
        
        $rules = [
            'subdomain_baru' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $hosting->update([
            'is_changed' => '1'
        ]);

        if ($request->jenis_hosting == 'VPS') {
            $hosting_baru = Hosting::create([
                'nama_kepala' => $request->nama_kepala,
                'nip_kepala' => $request->nip_kepala,
                'jenis_permohonan' => $request->jenis_permohonan,
                'user_id' => $request->user,
                'deskripsi_web' => $request->deskripsi_web,
                'jenis_hosting' => $request->jenis_hosting,
                'subdomain_baru' => $request->subdomain_baru,
                'storage' => $request->storage,
                'status' => $request->status,
                'os' => $request->os,
                'processor' => $request->processor,
                'ram' => $request->ram,
                'old_hosting_id' => $hosting->id,
                'no_hosting' => $request->no_hosting,
            ]);
        }elseif($request->jenis_hosting == 'Cpanel')
        {
            $hosting_baru = Hosting::create([
                'nama_kepala' => $request->nama_kepala,
                'nip_kepala' => $request->nip_kepala,
                'jenis_permohonan' => $request->jenis_permohonan,
                'user_id' => $request->user,
                'deskripsi_web' => $request->deskripsi_web,
                'jenis_hosting' => $request->jenis_hosting,
                'subdomain_baru' => $request->subdomain_baru,
                'storage' => $request->storage,
                'status' => $request->status,
                'os' => Null,
                'processor' => Null,
                'ram' => Null,
                'old_hosting_id' => $hosting->id,
                'no_hosting' => $request->no_hosting,
            ]);
        }

        Devweb::create([
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jabatan' => $request->jabatan,
            'email' => $request->email,
            'phone' => $request->phone,
            'hosting' => $hosting_baru->id,
        ]);

        return redirect('/hosting')->with('successPermohonan', 'Permohonan penambahan spesifikasi berhasil dibuat!');
    }

    public function FormBaru(Request $request, $id)
    {
        $hosting = Hosting::find($id);

        $rules = [
            'surat' => 'required|mimes:pdf|max:2048',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        $form_hosting = $request->file('surat');
        $nama_file = date('Ymdhis') . '_' . $request->file('surat')->getClientOriginalName();
        $form_hosting->move('form_hosting/', $nama_file);
        $hosting->update([
            'surat' => $nama_file,
        ]);

        return redirect('/hosting')->with('success', 'Formulir permohonan berhasil diupload!');
    }

    public function PerubahanStatus(Request $request, $id)
    {
        $hosting = Hosting::find($id);

        if ($request->status == 4) {
            Hosting::where('id', $hosting->old_hosting_id)->update([
                'is_changed' => null
            ]);
        }

        $hosting->update([
            'status' => $request->status,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('/hosting')->with('success', 'Status berhasil diubah!');
    }

    public function cetakHosting($id)
    {
        $filename = "hosting.pdf";
        $data = [
            'title' => 'Layanan Hosting',
            'h' => Hosting::with('statushosting', 'user', 'developer')->find($id),
        ];
        $html = view('Hosting.cetak', $data);

        $pdf = new TCPDF;

        $pdf::SetTitle('Hosting');
        $pdf::SetHeaderMargin(30); // set margin untuk header
        $pdf::AddPage('P', 'F4');
        $pdf::SetFont('times', '', 12);
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output($filename, 'I');
    }
}
