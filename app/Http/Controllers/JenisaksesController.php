<?php

namespace App\Http\Controllers;

use App\Models\Jenishak;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class JenisaksesController extends Controller
{
    public function index()
    {
        $data = [
            'jenisakses' => Jenishak::all()
        ];
        return view('hak_akses.manajemen', $data);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nama' => 'required'
        ]);
        
        if($validator->fails()){
            return back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        Jenishak::create([
            'nama' => $request->nama,
        ]);
        
        return redirect('/manajemen-akses')->with('success', 'Jenis akses berhasil ditambahkan!');

    }

    public function update(Request $request, $id)
    {
        $jenisakses = Jenishak::find($id);

        $validator = Validator::make($request->all(),[
            'nama' => 'required'
        ]);
        
        if($validator->fails()){
            return back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $jenisakses->update([
            'nama' => $request->nama,
        ]);
        
        return redirect('/manajemen-akses')->with('success', 'Jenis akses berhasil diubah!');

    }

    public function delete($id)
    {
        $jenisakses = Jenishak::find($id);
        $jenisakses->delete();
        return redirect('/manajemen-akses')->with('success', 'Jenis akses berhasil dihapus!');
    }

}
