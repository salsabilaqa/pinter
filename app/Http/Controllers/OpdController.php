<?php

namespace App\Http\Controllers;

use App\Models\Opd;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OpdController extends Controller
{
    public function index()
    {
        $data = [
            'opd' => Opd::all()
        ];
        return view('opd.opd', $data);
    }

    public function add(){
        return view('opd.add-opd');
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'email' => 'required|email:dns',
            'telp' => 'required',
            'alamat' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/opd/add')->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $validated = $validator->validate();

        Opd::create($validated);

        return redirect('/opd')->with('success', 'OPD berhasil ditambahkan!');
    }
    
    public function detail($id){
        $data = [
            'opd' => Opd::find($id),
            'listuser' => User::where('opd_kode', $id)->get(),
        ];
        return view('opd.detail-opd', $data);
    }

    public function edit($id){
        $data = [
            'opd' => Opd::find($id),
        ];
        return view('opd.edit-opd', $data);
    }

    public function update(Request $request, $id)
    {
        $opd = Opd::find($id);

        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'email' => 'required|email:dns',
            'telp' => 'required',
            'alamat' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/opd/edit/' . $opd->id)->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $validated = $validator->validate();

        $opd->update($validated);

        return redirect('/opd')->with('success', 'OPD berhasil diubah!');
    }

    public function delete($id)
    {
        $opd = Opd::find($id);
        $opd->delete();
        return redirect('/opd')->with('success', 'OPD berhasil dihapus!');
    }
}
