<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Models\Hakakses;
use App\Models\Jenishak;
use App\Models\Pihakketiga;
use Illuminate\Http\Request;
use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HakaksesController extends Controller
{
    public function index(Request $request)
    {
        if ($request->tahun) {
            if (Auth::user()->role_id == 2) {
                $hakakses = Hakakses::where('user_id', Auth::user()->id)->where(DB::raw('YEAR(created_at)'), $request->tahun)->latest()->get();
            } else {
                $hakakses = Hakakses::where(DB::raw('YEAR(created_at)'), $request->tahun)->latest()->get();
            }
        } else {
            if (Auth::user()->role_id == 2) {
                $hakakses = Hakakses::where('user_id', Auth::user()->id)->where(DB::raw('YEAR(created_at)'), now())->latest()->get();
            } else {
                $hakakses = Hakakses::where(DB::raw('YEAR(created_at)'), now())->latest()->get();
            }
        }
        
        $tahun = DB::table('hak_akses')->select(DB::raw('YEAR(created_at) as tahun'))->orderBy(DB::raw('YEAR(created_at)'), 'desc')->groupBy(DB::raw('YEAR(created_at)'))->pluck('tahun');
        
        $data = [
            'hakakses' => $hakakses,
            'tahun' => $tahun,
            'jenisakses' => Jenishak::all(),
            'status' => Status::all()
        ];
        return view('hak_akses.hakakses', $data);
    }

    public function add()
    {
        $no_hak_akses = Hakakses::generateNomor();
        return view('hak_akses.add-hakakses', [
            'jenisakses' => Jenishak::all(),
            'no_hak_akses' => $no_hak_akses
        ]);
    }

    public function create(Request $request)
    {    
        $rules = [
            'nama_perusahaan' => 'required|string',
            'nama' => 'required|string',
            'telp' => 'required|numeric',
            'jabatan' => 'required|string',
            'waktu' => 'required|string',
            'tanggal_awal' => 'required|date',
            'tanggal_akhir' => 'required|date',
        ];

        if($request->jenisakses_lain){
            $rules['jenisakses_lain'] = 'required|string';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/hakakses/add')->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        $pihakketiga = Pihakketiga::create([
            'nama_perusahaan' => $request->nama_perusahaan,
            'nama' => $request->nama,
            'telp' => $request->telp,
            'jabatan' => $request->jabatan,
        ]);

        if ($request->jenis_hak_akses == 'lainnya') {
            $jenisAkses = Jenishak::create([
                'nama' => $request->jenisakses_lain,
            ]);
            $valuejenakses = $jenisAkses->id;
        } else {
            $valuejenakses = $request->jenis_hak_akses;
        }

        Hakakses::create([
            'user_id' => $request->user_id,
            'pihak_ketiga' => $pihakketiga->id,
            'jenis_hak_akses' => $valuejenakses,
            'alasan' => $request->alasan,
            'waktu' => $request->waktu,
            'tanggal_awal' => $request->tanggal_awal,
            'tanggal_akhir' => $request->tanggal_akhir,
            'status' => $request->status,
            'no_hak_akses' => $request->no_hak_akses,
        ]);

        return redirect('/hakakses')->with('successPermohonan', 'Permintaan Hak Akses berhasil dibuat!');
    }

    public function edit($id)
    {
        return view('hak_akses.edit-hakakses', [
            'hakakses' => Hakakses::find($id),
            'jenisakses' => Jenishak::all(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $hakakses = Hakakses::find($id);

        $rules = [
            'nama_perusahaan' => 'required|string',
            'nama' => 'required|string',
            'telp' => 'required|numeric',
            'jabatan' => 'required|string',
            'waktu' => 'required|string',
            'tanggal_awal' => 'required|date',
            'tanggal_akhir' => 'required|date',
        ];

        if($request->jenisakses_lain){
            $rules['jenisakses_lain'] = 'required|string';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/hakakses/edit/'. $hakakses->id)->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        Pihakketiga::where('id', $hakakses->pihak_ketiga)->update([
            'nama_perusahaan' => $request->nama_perusahaan,
            'nama' => $request->nama,
            'telp' => $request->telp,
            'jabatan' => $request->jabatan,
        ]);

        if ($request->jenis_hak_akses == 'lainnya') {
            $jenisAkses = Jenishak::create([
                'nama' => $request->jenisakses_lain,
            ]);
            $valuejenakses = $jenisAkses->id;
        } else {
            $valuejenakses = $request->jenis_hak_akses;
        }

        $hakakses->update([
            'jenis_hak_akses' => $valuejenakses,
            'alasan' => $request->alasan,
            'waktu' => $request->waktu,
            'tanggal_awal' => $request->tanggal_awal,
            'tanggal_akhir' => $request->tanggal_akhir,
        ]);

        return redirect('/hakakses')->with('success', 'Permintaan hak akses berhasil diubah!');
    }

    public function uploadFormulir(Request $request, $id)
    {   
        $hakakses = Hakakses::find($id);

        $validator = Validator::make($request->all(), [
            'surat' => 'required|file|mimes:pdf|max:5120'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('surat')) {
            $nama_file = date('Ymdhis') . '_' . $request->file('surat')->getClientOriginalName();
            $request->file('surat')->move('form_akses/', $nama_file);
            if ($request->oldFile) {
                unlink('form_akses/'. $request->oldFile);
            }
        } else {
            $nama_file = $request->oldFile;
        }

        $hakakses->update([
            'surat' => $nama_file,
        ]);

        return redirect('/hakakses')->with('success', 'Formulir permohonan berhasil diupload!');
    }

    public function status(Request $request, $id)
    {
        $hakakses = Hakakses::find($id);
        $hakakses->update([
            'status' => $request->status,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('/hakakses')->with('success', 'Status berhasil diubah!');
    }

    public function delete($id)
    {
        $delete = Hakakses::find($id);
        $delete->delete();
        return redirect('/hakakses')->with('success', 'Permintaan hak akses berhasil dihapus!');
    }

    public function cetak($id)
    {
        $filename = "hak-akses.pdf";
        $data = [
            'title' => 'Formulir Permintaan Hak Akses',
            'hakakses' => Hakakses::find($id),
            'jenisakses' => Jenishak::all()
        ];
        $html = view('hak_akses.cetak', $data);

        $pdf = new TCPDF;

        $pdf::SetTitle('Hak Akses');
        $pdf::SetHeaderMargin(30); // set margin untuk header
        $pdf::AddPage('P', 'F4');
        $pdf::SetFont('times', '', 12);
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output($filename, 'I');
    }
}
